import React, {Component} from 'react';
import {GetCountries, SendNumber, sendOtp, Expire, Resend, RemoveError} from '../../store/actions/loginHandler';
import { Redirect } from 'react-router-dom';
import PopUp from '../popUp/popUp';
import WelcomeClass from './Welcome.module.css';
import {connect} from 'react-redux';
var x;
class Welcome extends Component {
    state = {
        nullAll: true,
        resend: false,
        SCodeError: false,
        numberInp: "",
        codeSelect: 1,
        loggedIn: false
    }
    /*Timer daha da optimallasdirilmalidir */
    countDownTimer = () =>{
        var countDownDate = new Date();
        countDownDate.setMinutes(countDownDate.getMinutes()+1);
        countDownDate.setSeconds(countDownDate.getSeconds()+34);
        x = setInterval(() => {
            var now = new Date().getTime();
            var distance = countDownDate - now;
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            try{
                if(!this.props.expired) document.getElementById("timer").textContent = "0"+ minutes + ":"+ seconds;
            }
            catch {
                clearInterval(x);
            }
            if (distance <= 0) {
                clearInterval(x);
                this.props.Expire();
                return;
            }
        }, 1000);
    }
    
    NullAll = () =>{
        if(this.state.nullAll){
            document.getElementById("Form").reset();
            this.countDownTimer();
            this.setState({numberInp: ""});
            this.setState({nullAll:false});
        }
    }
    Resend = () =>{
        clearInterval(x-1);
        clearInterval(x);
        this.props.Resend();
        this.countDownTimer();
    }
    btnHandler = (e) =>{
        e.preventDefault();
        if(!this.props.login.verification){
            this.props.sendOtp(this.state.codeSelect, this.state.numberInp);
            this.setState({nullAll:true});
            this.NullAll();
        }
        else {
            if(this.state.numberInp === ""){
                return;
            }
            this.props.SendNumber(this.state.numberInp);
        }
    }
    changeHandler = (e) => {
        this.setState({numberInp: e.target.value});
        this.props.RemoveError();
    }
    codeSelect = (e) =>{
        this.setState({
            codeSelect: e.target.value
        })
    }
    componentDidMount = () =>{
        let loggedIn = localStorage.getItem("loggedIn");
        if(loggedIn && loggedIn === "true"){
            this.setState({loggedIn:true});
        }
        else{
            this.setState({loggedIn:false});
        }
        this.props.getCountries();
    }
    render = () =>{
        const { verification, expired, name, number, countries, home, error, errorMessage, countryCode } = this.props.login;
        return (
        <div className = {WelcomeClass.Welcome}>
            {error?<PopUp errorMessage = {errorMessage}/>:null}
            {this.props.login.loggedIn || this.state.loggedIn? <Redirect to ="/home" />:null}
            <div className = {WelcomeClass.Circle}></div>
            <h1 className = {WelcomeClass.H1}> {verification? "Verification": "Welcome to Fooderos"}</h1>
            <p className = {WelcomeClass.P}>{verification? "Dear, "+name+" please enter your verification code which sent to +"+ countryCode + number: "Please enter your phone number and Log in"}</p>
            <div className = {WelcomeClass.userInp}>
                <form id = "Form" className = {WelcomeClass.Form}>
                    {!verification? 
                    <fieldset className = {WelcomeClass.countryCodes}>
                        <legend>Code</legend>
                        <select className = {WelcomeClass.Select} onChange = {this.codeSelect}>
                            {!verification?this.props.login.countries.map(country =>{
                                return(
                                        <option value = {country.id}>+{country.phone_code}</option>
                                )
                            }):null}
                        </select>
                    </fieldset>
                    :null}
                    <input onChange = {this.changeHandler} type = "text" className = {verification? (this.state.SCodeError || expired || error) ? [WelcomeClass.SCode, WelcomeClass.Error].join(' '):WelcomeClass.SCode: error ? [WelcomeClass.Number,WelcomeClass.Error].join(' '): WelcomeClass.Number} placeholder = {verification? "Security code": "Phone number"}/>
                    {verification && expired?<p className = {WelcomeClass.Expired}>Verification code expired. Try to resend</p>:null}
                    {verification && !expired?<p className = {WelcomeClass.Expires}>Code expires in: <span id = "timer" className = {WelcomeClass.Time}>01:33</span></p>:null }
                    <button disabled = {(expired && this.verification) || this.state.numberInp.trim() === ''} type= "submit" className = {expired || this.state.numberInp.trim() === ''? [WelcomeClass.btnLogin,WelcomeClass.disableBtn].join(' '): WelcomeClass.btnLogin} onClick = {this.btnHandler}>{verification? "Continue": "Log in"}</button>
                </form>
                {verification? <p className = {WelcomeClass.ntReceive}>Didn't receive a code? <a onClick = {this.Resend} className = {WelcomeClass.Resend} href = "#">Resend</a></p> : <p className = {WelcomeClass.Agree}>By signing in you agree with <a href= "#" className = {WelcomeClass.Terms}>Terms and Conditions</a> </p>}
            </div>
        </div> );
    }
}
export default connect(({login} ) => {
    return { login }
},dispatch =>{
    return {
        sendOtp: (code,number) => dispatch(sendOtp(code,number)),
        getCountries: () => dispatch(GetCountries()),
        SendNumber: (code) => dispatch(SendNumber(code)),
        Expire: () => dispatch(Expire()),
        Resend: () => dispatch(Resend()),
        RemoveError: () => dispatch(RemoveError())
    }
})(Welcome);