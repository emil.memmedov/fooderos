import {setNumber, setOtp, setCountries, expire, resend, error, removeError,logOut} from './actions';
import {Axios} from '../axios';
let loginInfo = {
    
}

const RestApi = {
    url: "https://dev-api.fooderos.com/api/", //main
    countries: "v1/countries", //for get country codes
    otp: "v1/send/otp", // to sent otp
    login: "v1/login", //for login with phone number
    logout: "v1/logout"
}
export const GetCountries = () =>{
    return dispatch=>{
        Axios.get(RestApi.countries)
        .then(response =>{
            dispatch({type: setCountries, data: response.data.content});
        })
    }
}
export const sendOtp = (code,number) =>{
    return dispatch =>{
        Axios.post(RestApi.otp,
        {
            phone: number,
            country_id: code
        })
        .then(response =>{
            dispatch({
                type: setOtp,
                payload: {
                    name:response.data.content.name,
                    number: number,
                    code: code
                }
            });
            loginInfo.code = code;
            loginInfo.number = number;
            loginInfo.name = response.data.content.name;
        }).catch(error=>{
            if (error.response.data.error){
                if(error.response.data.error.phone){
                    dispatch(Error(error.response.data.error.phone[0]));
                }
                else dispatch(Error(error.response.data.error));
            }
        });
    }
}
const Error = (message) => {
    return {
        type: error,
        message: message
    }
}
export const RemoveError = () => {
    return {
        type: removeError
    }
}
export const SendNumber = (code) => {
    return dispatch =>{
        Axios.post(RestApi.login,{
            country_id: loginInfo.code,
            phone: loginInfo.number,
            name: loginInfo.name,
            otp: code 
        })
        .then(response =>{
            dispatch({
                type: setNumber,
                payload: {
                    token: response.data.content.token,
                    expires_in: response.data.content.expires_in,
                    is_phone_verified: response.data.content.is_phone_verified,
                    module_permissions: response.data.content.module_permissions
                }
            });
        }).catch(error=>{
            if(error.response && error.response.data.error){
                dispatch(Error(error.response.data.error));
            }
        });
    }
}

export const Expire = ()=>{
    return {
        type: expire
    }
}
export const Resend = () =>{
    return dispatch =>{
        Axios.post(RestApi.otp,{
            phone: loginInfo.number,
            country_id: loginInfo.code
        })
        .then(response =>{
            dispatch({
                type: resend,
                name: response.data.content.name
            });
        });
    }
}
export const LogOut = () => {
    return dispatch =>{
        Axios.post(RestApi.logout).then(res=>{
            alert("dispatch")
            dispatch({type: logOut})
        })
    }
}