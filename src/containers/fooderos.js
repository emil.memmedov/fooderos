import React, {Component} from 'react';
import {Redirect, Route} from 'react-router-dom';
import Home from '../components/home/Home';
import Login from '../components/login/login';

class Fooderos extends Component {
    state = {  }
    render() { 
        return (  
        <React.Fragment>
            <Route path='/login' exact component={Login}/>
            <Route path='/home' exact component={Home}/>
            <Route path='/' > <Redirect to = "/login"/> </Route>
        </React.Fragment>);
    }
}
 
export default Fooderos;