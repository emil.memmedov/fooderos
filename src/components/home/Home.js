import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom';
import { LogOut } from '../../store/actions/loginHandler';


class Home extends Component {
    state = {  
        out:false
    }
    logOutHandler = () =>{
        localStorage.setItem("loggedIn","false");
        this.props.logOut();
        this.setState({out:true});
    }
    render() { 
        return (
        <div>
            {this.state.out? <Redirect to = "/login"/>:null}
            <h1>Welcome to Home</h1>
            <button onClick = {this.logOutHandler}>Log Out</button>
        </div> );
    }
}
 
export default connect(null,dispatch =>{
    return {
        logOut: () => dispatch(LogOut())
    }
})(Home);