import loginClass from './login.module.css';
import Triangle from './Triangle';
import Welcome from './Welcome';

const Login = () => {
    return ( 
    <div className = {loginClass.LoginPage}>
        <Triangle />
        <Welcome />
    </div>);
}
 
export default Login;