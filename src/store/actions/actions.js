export const setNumber = "SETNUMBER";
export const setOtp = "SETOTP";
export const setCountries = "SETCOUNTRIES";
export const expire = "EXPIRE";
export const resend = "RESEND";
export const error = "ERROR";
export const removeError = "REMOVEERROR";
export const logOut = "LOGOUT";