import {setNumber, setCountries, setOtp, expire, resend, error, removeError,logOut} from './actions/actions';

export const loginState = {
    countries: [],
    name: "",
    number: "",
    expired: false,
    verification: false,
    error: false,
    errorMessage: "",
    loggedIn: false
}

const loginReducer = (state = loginState,action) => {
    switch (action.type) {
        case setCountries:
            return {
                ...state, 
                countries: action.data};
        case setOtp:
            return{
                ...state,
                ...action.payload,
                countryCode: state.countries[action.payload.code-1].phone_code,
                verification: true
            }
        case setNumber:
            localStorage.setItem("loggedIn",true);
            return {
                ...state,
                ...action.payload,
                loggedIn:true
            }
        case expire:
            return{
                ...state,
                expired: true
            }
        case error:
            return {
                ...state,
                error: true,
                errorMessage: action.message
            }
        case removeError:
            return {
                ...state,
                error: false,
                errorMessage: ""
            }
            
        case resend:
            return {
                ...state,
                name: action.name,
                expired: false
            }
        case logOut:
            alert("ad");
            return{
                ...state,
                loggedIn:false
            };
        default: return state
    }
}

export default loginReducer;