import popUpClass from './popUp.module.css';

const PopUp = (props) =>{
    return ( 
    <div className = {[popUpClass.popUp,popUpClass.Drop].join(' ')}>
        <img className = {popUpClass.errorImg} src = "./Error-512.png"></img>
        <h1>Error</h1>
        <p>{props.errorMessage}</p>
    </div> );
}
export default PopUp;